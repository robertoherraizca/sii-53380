#include "glut.h"
#include "MundoServidor.h"
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

CMundo mundo;


void OnDraw(void); //esta funcion sera llamada para dibujar
void OnTimer(int value); //esta funcion sera llamada cuando transcurra una temporizacion
void OnKeyboardDown(unsigned char key, int x, int y); //cuando se pulse una tecla	


void signals (int sig) {
	if ( sig == SIGUSR1 ) {
		printf( "El servidor termino: 0\n" );
	}
	else {
		printf( "El servidor termino por la signal: %d\n", sig );
	}
	
	kill( getpid(), SIGKILL );
}



int main(int argc,char* argv[])
{
	
	struct sigaction accion;
	accion.sa_handler = &signals;
	accion.sa_flags = SA_RESTART;
	sigaction(SIGINT, &accion, NULL);
	sigaction(SIGTERM, &accion, NULL);
	sigaction(SIGPIPE , &accion, NULL);
	sigaction(SIGUSR1, &accion, NULL);


	//Inicializar el gestor de ventanas GLUT
	//y crear la ventana
	glutInit(&argc, argv);
	glutInitWindowSize(800,600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("Servidor");


	//Registrar los callbacks
	glutDisplayFunc(OnDraw);
	//glutMouseFunc(OnRaton);

	glutTimerFunc(25,OnTimer,0);//le decimos que dentro de 25ms llame 1 vez a la funcion OnTimer()
	glutKeyboardFunc(OnKeyboardDown);
	glutSetCursor(GLUT_CURSOR_FULL_CROSSHAIR);
	
	mundo.InitGL();
	
	//pasarle el control a GLUT,que llamara a los callbacks
	glutMainLoop();	

	return 0;   
}

void OnDraw(void)
{
	mundo.OnDraw();
}
void OnTimer(int value)
{
	mundo.OnTimer(value);
	glutTimerFunc(25,OnTimer,0);
	glutPostRedisplay();
}
void OnKeyboardDown(unsigned char key, int x, int y)
{
	mundo.OnKeyboardDown(key,x,y);
	glutPostRedisplay();
}
